package model;

import java.lang.Math;

public class Kotik {
    private int satiety = 0;
    private String name;
    private int age;
    private int weight;
    private String meow;
    private static int count = 0;

    public Kotik(String name, int age, String meow, int weight, int satiety) {
        this.name = name;
        this.age = age;
        this.meow = meow;
        this.weight = weight;
        this.satiety = satiety;
        count++;
    }

    public Kotik() {
        count++;
    }

    public void eat(int a) {
        satiety += a;
        System.out.println("Котик покушал");
    }

    public void eat(int b, String food) {
        satiety += b;
        System.out.println("Котик съел " + food);
    }

    public void eat() {
        this.eat(2, "Purina");
    }

    public boolean play() {
        if (satiety <= 0) {
            System.out.println("Котик голоден! Покормите котика!");
            return false;
        }
        satiety--;
        System.out.println("Котик поиграл");
        return true;
    }

    public boolean chaseMouse() {
        if (satiety <= 0) {
            System.out.println("Котик голоден! Покормите котика!");
            return false;
        }
        satiety--;
        System.out.println("Котик преследует мышь");
        return true;
    }

    public boolean sleep() {
        if (satiety <= 0) {
            System.out.println("Котик голоден! Покормите котика!");
            return false;
        }
        satiety--;
        System.out.println("Котик спит");
        return true;
    }

    public boolean wash() {
        if (satiety <= 0) {
            System.out.println("Котик голоден! Покормите котика!");
            return false;
        }
        satiety--;
        System.out.println("Котик умывается");
        return true;
    }

    public boolean walk() {
        if (satiety <= 0) {
            System.out.println("Котик голоден! Покормите котика!");
            return false;
        }
        satiety--;
        System.out.println("Котик гуляет");
        return true;

    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            if (satiety <= 0) {
                eat();
                continue;
            }
            switch ((int) (Math.random() * 5) + 1) {
                case 1:
                    play();
                    break;
                case 2:
                    chaseMouse();
                    break;
                case 3:
                    sleep();
                    break;
                case 4:
                    wash();
                    break;
                case 5:
                    walk();
                    break;
            }
        }
    }

    public int getWeight() {
        return weight;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getMeow() {
        return meow;
    }

    public int getSatiety() {
        return satiety;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public static int getCount() {
        System.out.println("Всего создано " + count + " котиков");
        return count;
    }

    public void setKotik(String name, int age, String meow, int weight, int satiety) {
        this.name = name;
        this.meow = meow;
        this.weight = weight;
        this.satiety = satiety;
    }
}