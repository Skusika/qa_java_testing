import model.Kotik;

public class Application {
    public static void main(String[] args) {
        System.out.println("Hello, model.Kotik!");
        Kotik mursik = new Kotik("Мурзик", 4, "мяу", 4, 2);
        System.out.println("Мы создали кота по имени " + mursik.getName() + ", его возраст - " + mursik.getAge() + ", его вес - " + mursik.getWeight());
        Kotik vasya = new Kotik();
        vasya.setKotik("Василий", 2, "мур", 5, 3);
        System.out.println("Мы создали кота по имени " + vasya.getName() + ", его возраст - " + vasya.getAge() + ", его вес - " + mursik.getWeight());

        if (mursik.getMeow().equals(vasya.getMeow())) {
            System.out.println("Котики разговаривают одинаково");
        } else {
            System.out.println("Котики разговаривают по-разному");
        }
        mursik.liveAnotherDay();
        Kotik.getCount();
    }
}

